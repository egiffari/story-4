from django.shortcuts import render, redirect
from . import forms
from .models import *

# Create your views here.   
def index(request):
    return render(request, 'index.html')

def index2(request):
    return render(request, 'index2.html')

def index3(request):
    return render(request, 'index3.html')

def index4(request):
    if request.method == 'POST':
        schedule_form = forms.Schedule(request.POST)
        if schedule_form.is_valid():
            new_schedule = Schedule(nama_kegiatan = request.POST['nama_kegiatan'], tempat = request.POST['tempat'], kategori = request.POST['kategori'], waktu = '{}-{}-{}'.format(request.POST['tanggal_year'], request.POST['tanggal_month'], request.POST['tanggal_day']), jam = request.POST['jam'])
            new_schedule.save()
            return redirect('/schedule/')
    else:
        schedules = Schedule.objects.all().order_by('waktu', 'jam')
        schedule_form = forms.Schedule()
    context = {'schedule': schedule_form, 'schedules': schedules}
    return render(request, 'index4.html', context)

def schedule_delete(request, id):
    schedule = Schedule.objects.get(id=id)
    schedule.delete()
    return redirect('homepage:index4')