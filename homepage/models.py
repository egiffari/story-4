from django.db import models

# Create your models here.
class Schedule(models.Model):
    nama_kegiatan = models.CharField(max_length = 20)
    tempat = models.CharField(max_length = 20)
    kategori = models.CharField(max_length = 20)
    waktu = models.DateField()
    jam = models.TimeField()