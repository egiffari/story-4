from django import forms

class Schedule(forms.Form):
    nama_kegiatan = forms.CharField(label = 'nama kegiatan', max_length = 20)
    tempat = forms.CharField(label = 'tempat', max_length = 20)
    kategori = forms.CharField(label = 'kategori', max_length = 20)
    tanggal = forms.DateField(widget = forms.SelectDateWidget)
    jam = forms.TimeField()
    

