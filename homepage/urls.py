from django.urls import path
from . import views


app_name = 'homepage'

urlpatterns = [
    path('scheduledelete/<id>', views.schedule_delete, name='schedule_delete'),
    path('schedule/', views.index4, name='index4'),
    path('profile/', views.index2, name='index2'),
    path('skills/', views.index3, name='index3'),
    path('', views.index, name='index')   
    
]